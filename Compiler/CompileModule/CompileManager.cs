﻿using System;

namespace CompileModule
{
    public class CompileManager
    {
        private string outputPath { get; set; }
        public CompileManager(string _outputPath)
        {
            this.outputPath = _outputPath;
        }
        
        public CompilationResult Compile(Languages language, string sourceCode)
        {
            BaseCompiler compiler;

            switch(language)
            {
                case Languages.Java:
                    {
                        compiler = new JavaCompiler();
                        break;
                    }
                case Languages.CSharp:
                    {
                        compiler = new CSharpCompiler();
                        break;
                    }
                case Languages.JavaScript:
                    {
                        compiler = new JavaScriptCompiler();
                        break;
                    }
                default:
                    {
                        throw new ArgumentOutOfRangeException();
                    }       
            }

            var compilationResult = compiler.Compile(sourceCode, this.outputPath);
            return compilationResult;
        }
    }
}
