﻿using System;
using System.Diagnostics;
using System.IO;

namespace CompileModule
{
    public abstract class BaseCompiler
    {
        public string workDir = "work";

        public abstract string FileTemplate { get; }
        public abstract string CompilerPath { get; }
        public abstract string FileExtension { get; }

        public string SaveSource(string source)
        {
            var curDir = Directory.GetCurrentDirectory();
            if(!Directory.Exists(Path.Combine(curDir, this.workDir)))
            {
                Directory.CreateDirectory(Path.Combine(curDir, this.workDir));
            }
            var fileName = string.Format("{0}.{1}", Guid.NewGuid().ToString(), FileExtension);
            var pathToFile = Path.Combine(curDir, this.workDir, fileName);
            File.WriteAllText(pathToFile, source);

            return fileName;
        }

        public ProcessStartInfo GetProccessStartInfo(string fileName)
        {
            return new ProcessStartInfo
            {
                WorkingDirectory = Path.Combine(Directory.GetCurrentDirectory(), this.workDir),
                CreateNoWindow = true,
                WindowStyle = ProcessWindowStyle.Hidden,
                RedirectStandardOutput = true,
                UseShellExecute = false,
                FileName = this.CompilerPath,
                Arguments = fileName
            };
        }

        public virtual CompilationResult Compile(string source, string outPath)
        {
            CompilationResult result = new CompilationResult();
            var fileName = this.SaveSource(this.FileTemplate.Replace("{source}", source));
            ProcessStartInfo startInfo = this.GetProccessStartInfo(fileName);
            var pathToFile = Path.Combine(Directory.GetCurrentDirectory(), this.workDir, fileName);

            try
            {
                using (Process exeProcess = Process.Start(startInfo))
                {
                    exeProcess.WaitForExit();
                    result.Message = exeProcess.StandardOutput.ReadToEnd();
                }

                var pathToResultFile = pathToFile.Replace(this.FileExtension, "exe");
                if (File.Exists(pathToResultFile))
                {
                    result.IsSuccess = true;
                    result.PathToFile = pathToResultFile;
                    result.FileName = fileName;
                }
                else
                {
                    result.IsSuccess = false;
                }
            }
            catch (Exception exc)
            {
                result.IsSuccess = false;
                result.Exception = exc.Message;
            }

            return result;
        }
    }
}
