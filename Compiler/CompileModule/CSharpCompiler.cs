﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;

namespace CompileModule
{
    public class CSharpCompiler : BaseCompiler
    {
        private string _fileTemplate = @"
            using System;

            public class Program
            {
                public static void Main(string[] args)
                {
                    var input = args.Length > 0 ? args[0] : """";
                    var result = ""your result"";

                    // write your code hear    
                    // read all input from 'input' variable
                    // write all output in result variable
                    {source}

                    Console.WriteLine(result);
                }
            }
        ";

        public override string FileTemplate => _fileTemplate;
        public override string CompilerPath => ConfigurationManager.AppSettings["СSC_PATH"];
        public override string FileExtension => "cs";

        public override CompilationResult Compile(string source, string outPath)
        {
            CompilationResult result = new CompilationResult();
            var fileName = this.SaveSource(this._fileTemplate.Replace("{source}", source));
            ProcessStartInfo startInfo = this.GetProccessStartInfo(fileName);
            var pathToFile = Path.Combine(Directory.GetCurrentDirectory(), this.workDir, fileName);

            try
            {
                using (Process exeProcess = Process.Start(startInfo))
                {
                    exeProcess.WaitForExit();
                    result.Message = exeProcess.StandardOutput.ReadToEnd();
                }

                var pathToResultFile = pathToFile.Replace(this.FileExtension, "exe");
                if (File.Exists(pathToResultFile))
                {
                    result.IsSuccess = true;
                    result.PathToFile = pathToResultFile;
                }
                else
                {
                    result.IsSuccess = false;
                }
            }
            catch (Exception exc)
            {
                result.IsSuccess = false;
                result.Exception = exc.Message;
            }

            return result;
        }
    }
}
