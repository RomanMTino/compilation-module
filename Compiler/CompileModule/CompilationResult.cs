﻿namespace CompileModule
{
    public class CompilationResult
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public string Exception { get; set; }
        public string PathToFile { get; set; }
        public string FileName { get; set; }
    }
}
