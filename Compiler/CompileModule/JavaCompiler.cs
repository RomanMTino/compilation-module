﻿using System;
using System.Configuration;

namespace CompileModule
{
    public class JavaCompiler : BaseCompiler
    {
        public override string FileTemplate => "{0}";
        public override string CompilerPath => ConfigurationManager.AppSettings["JVC_PATH"];
        public override string FileExtension => "java";

        public override CompilationResult Compile(string source, string outPath)
        {
            throw new NotImplementedException();
        }
    }
}
