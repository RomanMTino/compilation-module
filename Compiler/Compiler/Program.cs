﻿using CompileModule;
using System;
using System.Collections.Generic;

namespace Compiler
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var results = new List<CompilationResult>
            {
                CompileJS(),
                CompileCS()
            };

            foreach (var result in results)
            {

                if (!result.IsSuccess)
                {
                    Console.WriteLine("Compilation failed\n\n Message:\n" + result.Message, " Exception\n:" + result.Exception);
                }
                else
                {
                    Console.WriteLine("Compilation successful\n\n" + result.PathToFile);
                }
            }
        }

        public static CompilationResult CompileJS()
        {
            CompileManager compile = new CompileManager("C:\\Work\\CompileFolder");
            return compile.Compile(Languages.JavaScript, "result = 'Hello JavaScript I was compiled =);'");
        }

        public static CompilationResult CompileCS()
        {
            CompileManager compile = new CompileManager("C:\\Work\\CompileFolder");
            return compile.Compile(Languages.CSharp, "result = \"Hello CSharp I was compiled =)\";");
        }
    }
}
